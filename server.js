const express = require('express');
const srv = express();
let studentRoute = require('./routes/student');

const port = 4567;

srv.use(express.json());
srv.use(express.urlencoded({extended: true}));

srv.use('/student', studentRoute);

srv.use('/', express.static(__dirname + '/public'));

srv.get('/:name', (req, res) => {
    let name = req.params.name;
    res.send("<h1>Hi " + name + "!</h1>");
});

srv.get('/items',function(req,res){
    var id = req.query.id;
    res.send("<h1>" + id + "</h1>");
});

srv.post('/process', (req, res) => {
    let name = req.body.name;
    res.send("<h1>Welcome " + name + "!</h1>");
});

srv.use((req, res) => {
    res.send("<h1>Page not found.</h1>");
})

srv.listen(port, () => {
    console.log("Serving on http://localhost:" + port);
});