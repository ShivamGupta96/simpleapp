const route = require('express').Router();

let students = [
    {name: "Shivam", age: 21, year: "fourth"},
    {name: "Meghna", age: 19, year: "second"},
];

route.get('/', (req, res) => {
    res.send();
});

route.post('/addStudent', (req, res) => {
    let name = req.body.name;
    let age = req.body.age;
    let year = req.body.year;
    let studentObj = {};
    studentObj.name = name;
    studentObj.age = age;
    studentObj.year = year;
    students.push(studentObj);
    res.send(students);
})

module.exports = route;